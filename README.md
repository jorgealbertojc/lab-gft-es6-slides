# lab-gft-es6-slides

Slides del curso que se impartió en GFT sobre EcmaScriipt 6

### Visualización de la presentación

Se deben escribir los siguientes comandos:

```bash
npm install
```
Para levantar el servidor

```bash
npm start
```

Una vez hecho esto podrán visitar → _ [http://localhost:8000](http://localhost:8000) _
